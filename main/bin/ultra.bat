@echo off
REM ---------------------------------------------------------------------------
REM  Script for building/running ultra.
REM
REM Environment Variable Prequisites:
REM
REM   JAVA_HOME     Must point at your Java Development Kit installation.
REM
REM ---------------------------------------------------------------------------
REM $Id: ultra.bat,v 1.2 2001/07/02 01:35:22 hyoon Exp $


REM ----- Save Environment Variables That May Change --------------------------
if "%ULTRA_HOME%" == "" set ULTRA_HOME=F:\ultra\main
set _ULTRA_HOME=%ULTRA_HOME%
set _JAVAHELP_LIB=%_ULTRA_HOME%\lib\jhall.jar
set _PATH=%PATH%
set _CLASSPATH=%CLASSPATH%

REM ----- Verify and Set Required Environment Variables -----------------------
if "%JAVA_HOME%" == "" goto javahomeerror

REM ----- Set Up the Path ----------------------------------------
set PATH=%_ULTRA_HOME%\bin;%PATH%
REM echo Using PATH: %PATH%

REM ----- Set Up the Runtime Classpath ----------------------------------------
REM set CLASSPATH=%CLASSPATH%;%_ULTRA_HOME%\src
set CLASSPATH=%_ULTRA_HOME%\src;%_ULTRA_HOME%\tools\antlr-2.7.1
set CLASSPATH=%CLASSPATH%;%JAVA_HOME%\lib\tools.jar;%_JAVAHELP_LIB%
REM echo Using CLASSPATH: %CLASSPATH%

REM ----- Prepare Appropriate Java Execution Commands -------------------------
set _JAVAC=classic
set _START_CMD=start %JAVA_HOME%\bin\java -cp "%CLASSPATH%"
set _RUN_CMD=%JAVA_HOME%\bin\java -cp "%CLASSPATH%"
set _BUILD_CMD=%JAVA_HOME%\bin\javac -classpath "%CLASSPATH%"
set _JAR_CMD=%JAVA_HOME%\bin\jar
set _DOC_CMD=%JAVA_HOME%\bin\javadoc -classpath "%CLASSPATH%" -private -version -author -windowtitle "Ultra in Java API Specification" -doctitle "Ultra in Java"
if "%OS%" == "Windows_NT" set _CLEAN_CMD=del /S /Q
if "%OS%" == "" set _CLEAN_CMD=del

REM ----- Execute The Requested Command ---------------------------------------
if "%1" == "env" goto doEnv
if "%1" == "build" goto doBuild
if "%1" == "jar" goto doJar
if "%1" == "javadoc" goto doJavadoc
if "%1" == "clean" goto doClean
if "%1" == "run" goto doRun
if "%1" == "start" goto doStart
if "%1" == "stop" goto doStop

:doUsage
echo Usage:  ultra " env | build | jar | javadoc | clean | run | start | stop "
echo Commands:
echo   env     -  Set up environment variables that ultra would use
echo   build   -  Build all ultra classes and the jar file
echo   jar     -  Create ultra.jar
echo   javadoc -  Generate api docs.
echo   clean   -  Clean all ultra classes
echo   run     -  Start ultra in the current window
echo   start   -  Start ultra in a separate window
echo   stop    -  Stop ultra
goto cleanup

:doEnv
goto finish

:doBuild
echo Building ultra...
REM %_CLEAN_CMD% %_ULTRA_HOME%\src\bluecraft\ultra\*.class
REM %_BUILD_CMD% %_ULTRA_HOME%\src\bluecraft\ultra\file\*.java
REM %_BUILD_CMD% %_ULTRA_HOME%\src\bluecraft\ultra\resource\*.java
REM %_BUILD_CMD% %_ULTRA_HOME%\src\bluecraft\ultra\util\*.java
%_BUILD_CMD% %_ULTRA_HOME%\src\bluecraft\ultra\*.java
REM echo Done.
REM goto cleanup

:doJar
echo Creating jar file...
%_CLEAN_CMD% %_ULTRA_HOME%\src\bluecraft\ultra\*.*~
%_CLEAN_CMD% %_ULTRA_HOME%\build\ultra.jar
cd %_ULTRA_HOME%\src
%_JAR_CMD% cfm %_ULTRA_HOME%\build\ultra.jar %_ULTRA_HOME%\bin\ultra.mf -C %_ULTRA_HOME%\src\ bluecraft\ultra\*.class
REM bluecraft\ultra\file\*.class bluecraft\ultra\resource\*.class bluecraft\ultra\util\*.class bluecraft\ultra\images bluecraft\ultra\help
cd %_ULTRA_HOME%\bin
REM %_JAR_CMD% cvfm %_ULTRA_HOME%\build\ultra.jar %_ULTRA_HOME%\bin\ultra.mf -C %_ULTRA_HOME%\src\ %_ULTRA_HOME%\src\bluecraft\ultra\*.class %_ULTRA_HOME%\src\bluecraft\ultra\file\*.class %_ULTRA_HOME%\src\bluecraft\ultra\resource\*.class %_ULTRA_HOME%\src\bluecraft\ultra\util\*.class %_ULTRA_HOME%\src\bluecraft\ultra\images %_ULTRA_HOME%\src\bluecraft\ultra\help
echo Done.
goto cleanup

:doJavadoc
echo Creating API docs...
%_CLEAN_CMD% %_ULTRA_HOME%\docs\api\*.*
%_DOC_CMD% -d %_ULTRA_HOME%\docs\api -sourcepath %_ULTRA_HOME%\src @%_ULTRA_HOME%\bin\package.list
echo Done.
goto cleanup

:doClean
echo Cleaning up...
%_CLEAN_CMD% %_ULTRA_HOME%\src\bluecraft\ultra\*.*~
%_CLEAN_CMD% %_ULTRA_HOME%\src\bluecraft\ultra\*.class
echo Done.
goto cleanup

:doRun
cd %_ULTRA_HOME%
%_RUN_CMD% -DJAVAC=%_JAVAC% bluecraft.ultra.Ultra
REM %_RUN_CMD% -DJAVAC=%_JAVAC% bluecraft.ultra.WorkSheet
cd %_ULTRA_HOME%\bin
goto cleanup

:doStart
cd %_ULTRA_HOME%
%_START_CMD% -DJAVAC=%_JAVAC% bluecraft.ultra.Ultra
REM %_START_CMD% -DJAVAC=%_JAVAC% bluecraft.ultra.WorkSheet
cd %_ULTRA_HOME%\bin
goto cleanup

:doStop
echo Stop is not implemented yet.
goto cleanup

REM ----- Errors --------------------------------------------------------------
:javahomeerror
echo "ERROR: JAVA_HOME not found in your environment."
echo "Please, set the JAVA_HOME variable in your environment to match the"
echo "location of the Java Virtual Machine you want to use."

REM ----- Restore Environment Variables ---------------------------------------
:cleanup
set PATH=%_PATH%
set CLASSPATH=%_CLASSPATH%
set _PATH=
set _CLASSPATH=
set _JAVAC=
set _JAVAHELP_LIB=
set _ULTRA_HOME=
set _BUILD_CMD=
set _JAR_CMD=
set _DOC_CMD=
set _CLEAN_CMD=
set _RUN_CMD=
set _START_CMD=
:finish


