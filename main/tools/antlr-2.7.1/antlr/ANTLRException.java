package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ANTLRException.java,v 1.2 2001/07/02 01:35:23 hyoon Exp $
 */

public class ANTLRException extends Exception {

	public ANTLRException() {
		super();
	}
	public ANTLRException(String s) {
		super(s);
	}
}

