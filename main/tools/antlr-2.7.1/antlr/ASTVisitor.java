package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ASTVisitor.java,v 1.2 2001/07/02 01:35:23 hyoon Exp $
 */

import antlr.collections.AST;

public interface ASTVisitor{
    public void visit(AST node);
}

