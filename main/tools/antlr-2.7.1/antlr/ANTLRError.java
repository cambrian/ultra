package antlr;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ANTLRError.java,v 1.2 2001/07/02 01:35:23 hyoon Exp $
 */

public class ANTLRError extends Error {
    
    /**
     * ANTLRError constructor comment.
     */
    public ANTLRError() {
	super();
    }
    
    /**
     * ANTLRError constructor comment.
     * @param s java.lang.String
     */
    public ANTLRError(String s) {
	super(s);
    }
}

