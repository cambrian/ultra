package antlr.collections;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: ASTEnumeration.java,v 1.2 2001/07/02 01:35:24 hyoon Exp $
 */

public interface ASTEnumeration {
	public boolean hasMoreNodes();
	public AST nextNode();
}

