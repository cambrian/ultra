package antlr.collections.impl;

/* ANTLR Translator Generator
 * Project led by Terence Parr at http://www.jGuru.com
 * Software rights: http://www.antlr.org/RIGHTS.html
 *
 * $Id: IntRange.java,v 1.2 2001/07/02 01:35:24 hyoon Exp $
 */

public class IntRange {
	int begin, end;


	public IntRange(int begin, int end) {
		this.begin = begin;
		this.end = end;
	}
	public String toString() {
		return begin+".."+end;
	}
}

